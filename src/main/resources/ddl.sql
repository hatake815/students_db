CREATE TABLE public.students
(
    id integer NOT NULL,
    name character varying COLLATE pg_catalog."default",
    surname character varying COLLATE pg_catalog."default",
    "group" character varying COLLATE pg_catalog."default",
    CONSTRAINT students_pk PRIMARY KEY (id)
)
    WITH (
        OIDS = FALSE
    )
    TABLESPACE pg_default;

ALTER TABLE public.students
    OWNER to postgres;